﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apply.Flyer.WebApi.Services
{
    public class Sql
    {
        public const string SearchPaging =
            @"SELECT *
                FROM (SELECT F.*, ROW_NUMBER() OVER(ORDER BY F.Id) AS RowNumber
                    FROM  Foundation F
		            WHERE 1 = 1 @searchCondition) FoundationX
                WHERE 
		            RowNumber >= @StartRow
                    AND RowNumber < @EndRow
                ORDER BY Id";

    }
}