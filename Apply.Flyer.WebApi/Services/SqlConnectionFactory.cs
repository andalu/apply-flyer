﻿using System.Data;
using System.Data.SqlClient;

namespace Apply.Flyer.WebApi.Services
{
    public class SqlConnectionFactory : IConnectionFactory
    {
        public IDbConnection OpenConnection()
        {
            return new SqlConnection(ConnectionStringHelper.GetConnectionString());
        }
    }
}