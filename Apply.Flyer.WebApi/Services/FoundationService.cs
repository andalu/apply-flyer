﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Apply.Flyer.WebApi.Models;
using Dapper;

namespace Apply.Flyer.WebApi.Services
{
    public class FoundationService : ServiceBase, IFoundationService
    {
        public Foundation GetByAbbreviation(string shortname)
        {
            using (var cn = OpenConnection())
            {
                const string sql = @"SELECT TOP 1 * FROM Foundation
                                    WHERE Shortname = @shortname";
                return cn.Query<Foundation>(sql, new { shortname }).SingleOrDefault();
            }
        }

        public Foundation Get(long id)
        {
            using (var cn = OpenConnection())
            {
                const string sql = @"SELECT * FROM Foundation
                                    WHERE Id = @id";
                return cn.Query<Foundation>(sql, new { id }).SingleOrDefault();
            }
        }

        public Foundation Save(Foundation foundation)
        {
            const string insert = @"INSERT INTO [Foundation]
                                   ([Id]
                                   ,[Name]
                                   ,[Shortname]
                                   ,[FlyerContact]
                                   ,[FlyerContactPhone]
                                   ,[FlyerContactEmail]
                                   ,[FlyerViewCount]            
                                   ,[Updated]
		                           ,[Viewed])
                             VALUES
                                   (@id
                                   ,@name
                                   ,@shortname
                                   ,@flyerContact
                                   ,@flyerContactPhone
                                   ,@flyerContactEmail
                                   ,@flyerViewCount
                                   ,@updated
		                           ,@viewed)";

            const string updated = @"UPDATE [Foundation]
                                   SET [Name] = @name
                                      ,[FlyerContact] = @flyerContact
                                      ,[FlyerContactPhone] = @flyerContactPhone
                                      ,[FlyerContactEmail] = @flyerContactEmail
                                      ,[FlyerViewCount] = @flyerViewCount
                                      ,[Viewed] = @viewed
                                      ,[Updated] = @Updated
                                 WHERE Id = @Id";

            using (var cn = OpenConnection())
            {
                if (foundation.Id == 0)
                {
                    foundation.Id = GetId();
                    TrySetShortName(foundation);
                    cn.Execute(insert, foundation);
                }
                else
                {
                    TrySetShortName(foundation);
                    cn.Execute(updated, foundation);
                }
            }
            return foundation;
        }

        public IList<FoundationSearchModel> Search(string searchValue)
        {
            var sql = new StringBuilder("SELECT * FROM Foundation WHERE 1=1");
            var values = searchValue.Split(' ');
            foreach (var value in values)
            {
                if (string.IsNullOrEmpty(value.Trim())) continue;
                sql.Append($" AND purpose like '%{value}%'");
            }

            using (var cn = OpenConnection())
            {
                return cn.Query<FoundationSearchModel>(sql.ToString()).ToList();
            }
        }

        public IList<FoundationSearchModel> Search(string searchValue, int startRow, int endRow)
        {

            //poor mans paging sql :)
            var preConditionSql = @"SELECT *
                FROM (SELECT F.*, ROW_NUMBER() OVER(ORDER BY F.Id) AS RowNumber
                    FROM  Foundation F
		            WHERE 1 = 1 ";
            var postConditionSql = @") FoundationX
                WHERE RowNumber >= @StartRow AND RowNumber <= @EndRow
                ORDER BY Id";
            
            var searchCondition = new StringBuilder($" AND F.Name like '%{searchValue}%'");
            var values = searchValue.Split(' ');
            searchCondition.Append(" OR (");
            for (var i = 0; i < values.Length; i++)
            {
                if (string.IsNullOrEmpty(values[i].Trim())) continue;
                
                if (i == 0)
                {
                    searchCondition.Append($"F.purpose like '%{values[i]}%'");
                }
                else
                {
                    searchCondition.Append($" AND F.purpose like '%{values[i]}%'");
                }
            }
            
            searchCondition.Append(")");

            var sql = preConditionSql + searchCondition.ToString() + postConditionSql;
            using (var cn = OpenConnection())
            {
                return cn.Query<FoundationSearchModel>(sql, new {startRow, endRow}).ToList();
            }
        }

        private static void TrySetShortName(Foundation foundation)
        {
            if (!string.IsNullOrEmpty(foundation.Shortname)) return;
            var id = foundation.Id.ToString();
            foundation.Shortname = id.Length > 6 ? id.Substring(id.Length - 4) : id;
        }

        private long GetId()
        {
            using (var cn = OpenConnection())
            {
                const string sql = @"SELECT MAX(Id)+1 FROM Foundation";
                return cn.Query<int>(sql).SingleOrDefault();
            }
        }
    }
}