using System.Collections.Generic;
using Apply.Flyer.WebApi.Models;

namespace Apply.Flyer.WebApi.Services
{
    public interface IFoundationService
    {
        Foundation Get(long id);
        Foundation GetByAbbreviation(string abbreviation);
        Foundation Save(Foundation foundation);
        IList<FoundationSearchModel> Search(string searchValue);
        IList<FoundationSearchModel> Search(string searchValue, int startRow, int endRow);

    }
}