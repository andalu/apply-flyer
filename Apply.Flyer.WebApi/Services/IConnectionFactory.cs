﻿using System.Data;

namespace Apply.Flyer.WebApi.Services
{
    public interface IConnectionFactory
    {
        IDbConnection OpenConnection();
    }
}