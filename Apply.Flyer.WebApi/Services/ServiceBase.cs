﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Apply.Flyer.WebApi.Services
{
    public abstract class ServiceBase
    {
        private static readonly IConnectionFactory connectionFactory = new SqlConnectionFactory();

        protected static IDbConnection OpenConnection()
        {
            return connectionFactory.OpenConnection();
        }
    }
}