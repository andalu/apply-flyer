using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: CLSCompliantAttribute(true )]
[assembly: ComVisibleAttribute(false)]
[assembly: AssemblyTitleAttribute(" 0.0.0.0")]
[assembly: AssemblyDescriptionAttribute("")]
[assembly: AssemblyCompanyAttribute("Nobel Biocare")]
[assembly: AssemblyProductAttribute(" 0.0.0.0")]
[assembly: AssemblyCopyrightAttribute("Nobel Biocare")]
[assembly: AssemblyVersionAttribute("0.0.0.0")]
[assembly: AssemblyInformationalVersionAttribute("0.0.0.0 / e512754")]
[assembly: AssemblyFileVersionAttribute("0.0.0.0")]
[assembly: AssemblyDelaySignAttribute(false)]

