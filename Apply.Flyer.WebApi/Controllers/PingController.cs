﻿using System.Web.Http;

namespace Apply.Flyer.WebApi.Controllers
{
    public class PingController : ApiController
    {
        public IHttpActionResult Get()
        {
            return Ok(new {Ping=true});
        }
    }
}