﻿using System;
using System.Web.Http;
using Apply.Flyer.Messages;
using Apply.Flyer.WebApi.Models;
using Apply.Flyer.WebApi.Services;

namespace Apply.Flyer.WebApi.Controllers
{
    [RoutePrefix("api/foundations")]
    public class FoundationsController : ApiController
    {
        private readonly IFoundationService service;

        public FoundationsController(IFoundationService service)
        {
            this.service = service;
        }

        public IHttpActionResult Get(long id)
        {

            return Ok(service.Get(id));
        }

        [Route("shortname/{shortname}")]
        public IHttpActionResult GetByAbbreviation(string shortname)
        {
            return Ok(service.GetByAbbreviation(shortname) ?? new Foundation());
        }

        [Route("search/{searchvalue}")]
        [HttpGet]
        public IHttpActionResult Search(string searchvalue)
        {
            return Ok(service.Search(searchvalue));
        }

        [Route("search/{searchvalue}/{startRow}/{endRow}")]
        [HttpGet]
        public IHttpActionResult SearchPaging(string searchvalue, int startRow, int endRow)
        {
            var res = Ok(service.Search(searchvalue, startRow, endRow));
            return res;
        }

        public IHttpActionResult Post(Foundation model)
        {
            if (model == null)
            {
                return BadRequest("No foundation provided");
            }
            model.Updated = DateTime.Now;
            service.Save(model);
            Bus.Send("apply.flyer.service", CreateFoundationUpdatedMessage(model));
            return Ok(model);
        }

        [Route("view")]
        [HttpPost]
        public IHttpActionResult SetFoundationViewed(FoundationViewedModel model)
        {
            if (model == null)
            {
                return BadRequest("No foundation provided");
            }
            var foundation = service.Get(model.Id);
            foundation.Viewed = DateTime.Now;
            foundation.FlyerViewCount++;
            service.Save(foundation);
            if (foundation.FlyerViewCount<2)
            {
                Bus.Send("apply.flyer.service", CreateFoundationViewedMessage(foundation));
            }
            return Ok(foundation);
        }
        private FoundationUpdated CreateFoundationUpdatedMessage(Foundation foundation)
        {
            return new FoundationUpdated
            {
                Name = foundation.Name,
                Shortname = foundation.Shortname,
                FlyerContact = foundation.FlyerContact,
                FlyerContactPhone = foundation.FlyerContactPhone,
                FlyerContactEmail = foundation.FlyerContactEmail,
                Date = foundation.Updated ?? DateTime.Now
            };
        }

        private FoundationViewed CreateFoundationViewedMessage(Foundation foundation)
        {
            return new FoundationViewed()
            {
                Name = foundation.Name,
                Shortname = foundation.Shortname,
                FlyerContact = foundation.FlyerContact,
                FlyerContactPhone = foundation.FlyerContactPhone,
                FlyerContactEmail = foundation.FlyerContactEmail,
                Date = foundation.Viewed ?? DateTime.Now,
            };
        }
    }
}