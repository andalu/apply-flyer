﻿using System.Collections.Generic;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Cors;
using Apply.Flyer.WebApi;
using Apply.Flyer.WebApi.Infrastructure;
using EzBus.Logging;
using EzBus.Msmq;
using Microsoft.Owin;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace Apply.Flyer.WebApi
{
    public class Startup
    {
        private readonly HttpConfiguration config = new HttpConfiguration();

        public void Configuration(IAppBuilder app)
        {
            ConfigureAutoMapper();
            ConfigureJson();
            ConfigureContainer();
            ConfigureRoutes();
            ConfigurCors();

            app.UseWebApi(config);

            Bus.Configure(x =>
            {
                x.EndpointName = "Apply.Flyer.WebApi";
                x.LogLevel = LogLevel.Info;

            }).UseMsmq();
        }

        private void ConfigureRoutes()
        {
            config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute("api", "api/{controller}/{id}", new { id = RouteParameter.Optional });
        }

        private void ConfigureContainer()
        {
            var container = new StructureMap.Container(x =>
            {
                x.Scan(s =>
                {
                    s.LookForRegistries();
                    s.AssembliesFromApplicationBaseDirectory();
                });
            });

            config.DependencyResolver = new StructureMapDependencyResolver(container);
        }

        private void ConfigureJson()
        {
            var defaultSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                Converters = new List<JsonConverter>
                {
                    new StringEnumConverter {CamelCaseText = true},
                }
            };

            JsonConvert.DefaultSettings = () => defaultSettings;

            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            config.Formatters.JsonFormatter.SerializerSettings = defaultSettings;
        }
        private void ConfigurCors()
        {
            config.EnableCors(new EnableCorsAttribute("*", "*", "*"));
        }
        private static void ConfigureAutoMapper()
        {
            //Mapper.Initialize(cfg => cfg.CreateMap<InspectionPointRecordModel, InspectionPointRecord>());
        }
    }
}
