﻿using Apply.Flyer.WebApi.Services;
using StructureMap.Configuration.DSL;

namespace Apply.Flyer.WebApi.Infrastructure
{
    public class ServiceRegistry : Registry
    {
        public ServiceRegistry()
        {
            For<IFoundationService>().Use<FoundationService>();
        }
    }
}