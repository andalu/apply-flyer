﻿using System;

namespace Apply.Flyer.WebApi.Models
{
    public class Foundation
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Shortname { get; set; }
        public string Ssn { get; set; }
        public string AddressCo { get; set; }
        public string Address { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }
        public string Contact { get; set; }
        public string Year { get; set; }
        public string Purpose { get; set; }
        public string Assets { get; set; }
        public string Webpage { get; set; }
        public string Email { get; set; }
        public string FlyerContact { get; set; }
        public string FlyerContactPhone { get; set; }
        public string FlyerContactEmail { get; set; }
        public int FlyerViewCount { get; set; }
        public DateTime? Viewed { get; set; }
        public DateTime? Updated { get; set; }
    }
}