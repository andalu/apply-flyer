namespace Apply.Flyer.WebApi.Models
{
    public class FoundationSearchModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Shortname { get; set; }
        public string Ssn { get; set; }
        public string AddressCo { get; set; }
        public string Address { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public string Phone { get; set; }
        public string Contact { get; set; }
        public string Year { get; set; }
        public string Purpose { get; set; }
        public string Assets { get; set; }
        public string Webpage { get; set; }
        public string Email { get; set; }
    }
}