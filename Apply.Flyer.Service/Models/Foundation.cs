﻿using System;

namespace Apply.Flyer.Service.Models
{
    public class Foundation
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Shortname { get; set; }
        public string FlyerContact { get; set; }
        public string FlyerContactPhone { get; set; }
        public string FlyerContactEmail { get; set; }
        public int FlyerViewCount { get; set; }
        public DateTime? Viewed { get; set; }
        public DateTime? Updated { get; set; }
    }
}
