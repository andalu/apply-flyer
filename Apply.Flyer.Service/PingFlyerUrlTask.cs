using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Timers;
using Apply.Flyer.Service.Models;
using EzBus;
using Flurl;
using Flurl.Http;
using log4net;

namespace Apply.Flyer.Service
{
    public class PingFlyerUrlTask : IStartupTask
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ServiceHost));
        private readonly string baseUrl;
        private string baseApiUrl;
        private Timer timer;


        public PingFlyerUrlTask()
        {
            baseUrl = ConfigurationManager.AppSettings["apply:flyer:url"];
            baseApiUrl = ConfigurationManager.AppSettings["apply:flyer:apiurl"];
        }

        public void Run()
        {
            timer = new System.Timers.Timer
            {
                AutoReset = true,
                Enabled = true,
                Interval = 120000
            };
            PingFlyerWeb();
            PingFlyerApi();
            timer.Elapsed += OnTimerElapsed;
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                timer.Enabled = false;
                PingFlyerWeb();
                PingFlyerApi();
            }
            catch (Exception exception)
            {
                Log.Error(exception);
            }
            finally
            {
                timer.Enabled = true;
            }
        }

        private void PingFlyerWeb()
        {
            var request = WebRequest.Create(baseUrl);
            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                using (var stream = response.GetResponseStream())
                {
                    var reader = new StreamReader(stream, Encoding.UTF8);
                    var responseString = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Log.Warn("Failed to ping flyer web...", ex);
                return;
            }
            Log.DebugFormat("Ping flyer web OK");
        }

        private void PingFlyerApi()
        {
            var foundation = Url(null, "foundations", "shortname", "zpt").GetJsonAsync<Foundation>().Result;
            if (foundation==null)
            {
                Log.WarnFormat("Ping flyer api failed...foundation not found");
                return;
            }
            Log.DebugFormat("Ping flyer api OK");
        }

        public string Name => "PingFlyerUrlTask";

        private IFlurlRequest Url(object values = null, params object[] segments)
        {
            return baseApiUrl
                .AppendPathSegments(segments)
                .SetQueryParams(values)
                .WithHeader("Accept", "application/json");
        }
    }
}