﻿using System;
using Topshelf;

namespace Apply.Flyer.Service
{
    class Program
    {
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();

            HostFactory.Run(x =>
            {
                x.Service<ServiceHost>(s =>
                {
                    s.ConstructUsing(name => new ServiceHost());
                    s.WhenStarted(host => host.Start());
                    s.WhenStopped(host => host.Stop());
                });

                x.RunAsLocalSystem();
                x.SetDescription("Apply Flyer Service");
                x.SetDisplayName("Apply Flyer Service");
                x.SetServiceName(typeof(Program).Namespace);
            });

            Console.WriteLine("...");
            Console.ReadLine();
        }
    }
}
