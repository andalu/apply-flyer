using EzBus.Msmq;
using log4net;

namespace Apply.Flyer.Service
{
    public class ServiceHost
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ServiceHost));
        
        public void Start()
        {
            Log.DebugFormat("Start Service.");
            Bus.Configure().UseMsmq();
        }
        public void Stop()
        {
            Log.Debug("Stop Service");
        }
    }
}