using EzBus.ObjectFactory;

namespace Apply.Flyer.Service
{
    public class CoreRegistry : ServiceRegistry
    {
        public CoreRegistry()
        {
            Register<IEmailSender, Smtp2GoEmailSender>();
        }
    }
}