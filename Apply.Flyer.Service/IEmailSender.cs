namespace Apply.Flyer.Service
{
    public interface IEmailSender
    {
        void Send(string subject, string body, string @from, string to, string customerReference, string bcc = null);
    }
}