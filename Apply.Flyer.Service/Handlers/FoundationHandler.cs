﻿using Apply.Flyer.Messages;
using EzBus;

namespace Apply.Flyer.Service.Handlers
{
    public class FoundationHandler : 
                IHandle<FoundationViewed>,
                IHandle<FoundationUpdated>
    {
        private readonly IEmailSender emailSender;

        public FoundationHandler(IEmailSender emailSender)
        {
            this.emailSender = emailSender;
        }
        public void Handle(FoundationViewed message)
        {
            var url = $"my.apply.se/{message.Shortname.ToLower()}";
            var from = $"\"my.apply.se/{message.Shortname.ToLower()}\" <noreply@apply.se>";
            var subjectFoundationName = CreateSubjectFoundationName(message);
            var subject = $"Apply Flyer Viewed by {subjectFoundationName}";
            var body = CreateBody(url, "Någon från denna stiftelse har alltså <strong>surfat in</strong> på sin länk! Kul!", message);

            emailSender.Send(subject, body, from, "apply@zapote.se", message.Shortname);
        }

        public void Handle(FoundationUpdated message)
        {
            var url = $"my.apply.se/{message.Shortname.ToLower()}";
            var from = $"\"my.apply.se/{message.Shortname.ToLower()}\" <noreply@apply.se>";
            var subjectFoundationName = CreateSubjectFoundationName(message);
            var subject = $"Apply Flyer Updated by {subjectFoundationName}";
            var body = CreateBody(url, "Någon från denna stiftelse har alltså <strong>uppdaterat</strong> sina kontaktuppgifter! Kul!", message);

            emailSender.Send(subject, body, from, "apply@zapote.se", message.Shortname);
        }

        private string CreateSubjectFoundationName(FoundationMessageBase message)
        {
            return message.Name.Length > 20 ? message.Name.Substring(0, 19) + "..." : message.Name;
        }

        private string CreateBody(string url, string headline, FoundationMessageBase message)
        {
            return $"<h3>{message.Name}</h3><p>{headline}</p>Short name: {message.Shortname}<br>Kontakt: {message.FlyerContact}<br>Telefon: {message.FlyerContactPhone}<br>E-post: {message.FlyerContactEmail}<br>Datum: {message.Date}<br><a href=http://{url} target=_blank>{url}</a><br><br>Hälsningar<br>Apply Flyer kommittéen";
        }
    }
}
