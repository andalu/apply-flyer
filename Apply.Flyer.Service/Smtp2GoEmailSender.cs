using System;
using System.Configuration;
using System.Dynamic;
using EzBus.Utils;
using Flurl.Http;
using log4net;

namespace Apply.Flyer.Service
{
    public class Smtp2GoEmailSender : IEmailSender
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Smtp2GoEmailSender));
        private readonly string apiKey;
        private readonly string resourceUrl;

        public Smtp2GoEmailSender()
        {
            apiKey = ConfigurationManager.AppSettings["smtp2go::apikey"];
            resourceUrl = ConfigurationManager.AppSettings["smtp2go::url"];
        }

        public void Send(string subject, string body, string @from, string to, string customerReference, string bcc = null)
        {
            try
            {
                dynamic request = new ExpandoObject();
                request.api_key = apiKey;
                request.to = new[] { to };
                request.subject = subject;
                request.html_body = body;
                request.text_body = body;
                request.sender = @from;

                if (customerReference.HasValue())
                {
                    request.custom_headers = new[]
                    {
                        new {header = "customer", value = customerReference}
                    };
                }

                if (bcc.HasValue())
                {
                    request.bcc = new[] { bcc };
                }

                var result = resourceUrl
                    .PostJsonAsync((object)request)
                    .ReceiveJson<dynamic>()
                    .Result;

                if (result.data.failed == 0) return;

                throw new Exception($"Failed to send mail to {to}. {result.data.error}");
            }
            catch (Exception ex)
            {
                log.Error("failed to email", ex);
                throw;
            }
        }
    }
}