﻿using System;

namespace Apply.Flyer.Messages
{
    public abstract class FoundationMessageBase
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Shortname { get; set; }
        public string FlyerContact { get; set; }
        public string FlyerContactPhone { get; set; }
        public string FlyerContactEmail { get; set; }
        public DateTime Date { get; set; }
    }
}