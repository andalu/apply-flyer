using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace Apply.Flyer.ShortNameGenerator
{
    public class FoundationService
    {
        private readonly char[] alphaNumberic = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
        private readonly List<string> inAppropriates;
        private static Random random = new Random();
        

        public FoundationService()
        {
            inAppropriates = Words.InAppropriates;
        }

        public bool Done { get; private set; }
        
        public long UpdateShortNames(long startId, int rows)
        {

            long currentId = 0;
            var foundations = GetFoundations(startId, rows);

            foreach (var foundation in foundations)
            {
                currentId = foundation.Id;
                if (String.IsNullOrEmpty(foundation.Name)) continue;
                var name = foundation.Name.Length > 100 ? foundation.Name.Substring(0, 99) : foundation.Name;
                name = foundation.Name.ReplaceMega(" av ", " ").ReplaceMega(" f�r ", " ").ReplaceMega(" och ", " ").ReplaceMega("�", "a").ReplaceMega("�", "a").ReplaceMega("�", "o");
                foundation.Shortname = GetShortName(name);

                foundation.Shortname = TryAdjustShortNameLenght(foundation.Shortname);
                int updateCount = 0;
                var updated = false;
                do
                {
                    updateCount++;
                    foundation.Shortname = ValidateShortName(foundation);
                    updated = TryUpdate(foundation);
                    if (!updated)
                    {
                        if (updateCount<10)
                        {
                            foundation.Shortname = TryAdjustShortNameLenght(foundation.Shortname.Substring(0, 2));
                            continue;
                        }
                        if (updateCount < 50)
                        {
                            foundation.Shortname = TryAdjustShortNameLenght(foundation.Shortname.Substring(0, 1));
                            continue;
                        }
                        foundation.Shortname = TryAdjustShortNameLenght(foundation.Shortname.Substring(0, 0));
                    }
                    
                } while (!updated);
                
                //TryUpdate(foundation);
                //Save(foundation);
            }
            
            Done = rows > foundations.Count;
            
            Console.WriteLine("klart");
            return currentId;
        }

        private List<Foundation> GetFoundations(long startId, int rows)
        {
            using (var cn = OpenConnection())
            {
                string sql = $"SELECT TOP {rows} * FROM Foundation WHERE Id > @startId AND ShortName is NULL ORDER BY 1";
                return cn.Query<Foundation>(sql, new { startId }).ToList();
            }
        }

        private Foundation Save(Foundation foundation)
        {
            const string updated = @"UPDATE [Foundation]
                                   SET [ShortName] = @shortName
                                 WHERE Id = @Id";

            using (var cn = OpenConnection())
            {
                cn.Execute(updated, foundation);
            }
            return foundation;
        }

        private IDbConnection OpenConnection()
        {
            return new SqlConnection(ConnectionStringHelper.GetConnectionString());
        }

        private bool ShortNameExists(Foundation foundation)
        {
            //return ShortNames.ContainsKey(foundation.Shortname);
            using (var cn = OpenConnection())
            {
                const string sql = @"SELECT count(1) FROM Foundation
                                    WHERE ShortName=@shortName AND Id<>@id";
                return cn.ExecuteScalar<bool>(sql, foundation);
            }
        }

        private bool TryUpdate(Foundation foundation)
        {
            using (var cn = OpenConnection())
            {
                const string sql = @"IF EXISTS(SELECT * FROM Foundation WHERE ShortName=@shortName)
                                    BEGIN	
	                                    select 0
                                    END
                                    ELSE
                                    BEGIN
	                                    UPDATE [Foundation]
		                                    SET [ShortName] = @shortName
                                        WHERE Id = @id
	                                    select 1
                                    END";
                return cn.Query<bool>(sql, foundation).SingleOrDefault();
            }
        }

        private string GetShortName(string name)
        {
            var nameArray = name.Split(' ');
            var shortName = String.Empty;
            foreach (var namePart in nameArray)
            {
                var firstChar = namePart.FirstOrDefault();
                if (!alphaNumberic.Any(x => x == firstChar))
                {
                    firstChar = GetRandomChar();
                }
                shortName += firstChar;
            }
            return shortName.ToUpper();
        }

        private string ValidateShortName(Foundation foundation)
        {
            TryAdjustShortNameLenght(foundation.Shortname);
            if (InApropriate(foundation.Shortname))
            {
                foundation.Shortname = TryAdjustShortNameLenght(foundation.Shortname.Substring(0, 2));
            }
            return foundation.Shortname;
        }

        private string AdjustShortName(string shortName)
        {
            shortName = TryAdjustShortNameLenght(shortName);
            shortName += GetRandomChar();
            return shortName;
        }

        private string TryAdjustShortNameLenght(string shortName)
        {
            const int max = 3;
            if (shortName.Length == max) return shortName;
            if (shortName.Length > max) return shortName.Substring(0, max);

            var chars = max - shortName.Length;

            for (var i = 0; i < chars; i++)
            {
                shortName += GetRandomChar();
            }
            return shortName;
        }

        private bool InApropriate(string shortName)
        {
            foreach (var inAppropriate in inAppropriates)
            {
                if (inAppropriate == shortName) return true;
            }
            return false;
        }

        private char GetRandomChar()
        {
            var num = random.Next(0, alphaNumberic.Length);
            return alphaNumberic[num];
        }
    }
}