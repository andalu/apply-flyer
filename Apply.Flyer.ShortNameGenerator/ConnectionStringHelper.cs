using System;
using System.Configuration;

namespace Apply.Flyer.ShortNameGenerator
{
    public class ConnectionStringHelper
    {
        public static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings[GetConnectionStringKey()].ConnectionString;
        }

        private static string GetConnectionStringKey()
        {
            return $"apply:foundation:db:{Environment.MachineName}";
        }
    }
}