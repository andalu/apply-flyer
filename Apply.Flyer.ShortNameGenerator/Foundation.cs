﻿namespace Apply.Flyer.ShortNameGenerator
{

    public class Foundation
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Shortname { get; set; }
    }
}
