import { inject } from 'aurelia-framework'
import { HttpClient, json } from 'aurelia-http-client';
import environment from '../environment';

@inject(HttpClient)
export class FoundationService {
  constructor(http) {
    this.http = http;
    http.configure(x => {
      x.withBaseUrl(environment.api);
    });
  }

  get(id) {
    return new Promise((resolve, reject) => {
      this.http.get('foundations/' + id)
        .then(httpMessage => JSON.parse(httpMessage.response))
        .then(foundation => resolve(foundation));
    });
  }

  getByShortName(shortName) {
    return new Promise((resolve, reject) => {
      this.http.get('foundations/shortName/' + shortName)
        .then(httpMessage => JSON.parse(httpMessage.response))
        .then(foundation => resolve(foundation));
    });
  }

  post(foundation) {
    return new Promise((resolve, reject) => {
        this.http.post('foundations', foundation)
        .then(reponse => resolve(reponse))
        .catch(e => reject(e));
    })
  }

  updateViewed(foundation) {
    return new Promise((resolve, reject) => {
        this.http.post('foundations/view', foundation)
        .then(reponse => resolve(reponse))
        .catch(e => reject(e));
    })
  }

}
