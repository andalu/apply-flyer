import { inject } from 'aurelia-framework';
import { FoundationService } from './services/foundation-service';
import { Router } from 'aurelia-router';

@inject(FoundationService, Router)
export class FlyerStart {
  constructor(foundationService, router) {
    this.foundationService = foundationService;
    this.router = router;
    
  }

  activate(params, router) {
    this.router = router;
    this.saved = null;
    this.shortName = params.id;
    if(this.shortName == null){
      this.foundation = {};
      return;
    }
    return this.foundationService.getByShortName(params.id).then(foundation => {
      this.foundation = foundation;
      this.saveViewed(foundation);      
      this.contactFocus = true;
    });
  }

  get canSave(){
    if (this.foundation.name){
      return true;
    }
   return false;
 } 

  save() {
    return new Promise((resolve, reject) => {
      this.foundationService.post(this.foundation)
      .then(httpMessage => JSON.parse(httpMessage.response))
      .then(foundation => {
        console.log('sparat');
        this.saved = true;
        resolve(foundation)
        console.log(foundation);
      })
      .catch(e => reject(e));
    });
    
  }

  saveViewed() {
      this.foundationService.updateViewed(this.foundation)
      .then(httpMessage => JSON.parse(httpMessage.response))
      .then(foundation => {
        this.foundation = foundation;
      })
      .catch(e => reject(e));
    
  }

  wrong(){
    this.wrongFoundation = this.foundation;
    this.foundation = {name: "",
                      flyerContact: this.wrongFoundation.flyerContact, 
                      flyerContactPhone: this.wrongFoundation.flyerContactEmail,
                      flyerContactEmail: this.wrongFoundation.flyerContactEmail};
    this.contactFocus = false;
    this.nameFocus = true;
  }

  wrongRegret(){
    this.foundation = this.wrongFoundation;
    this.wrongFoundation = null;
    this.nameFocus = false;
    this.contactFocus = true;
  }
}
