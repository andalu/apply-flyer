import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';
import environment from './environment';
import { PLATFORM } from 'aurelia-pal';
import 'babel-polyfill';
import * as Bluebird from 'bluebird';

// remove out if you don't want a Promise polyfill (remove also from webpack.config.js)
Bluebird.config({ warnings: { wForgottenReturn: false } });

export function configure(aurelia) {

  if (environment.prod) {
    aurelia.use.plugin('aurelia-google-analytics', config => {
      config.init('UA-25948688-3', 'ga');
      config.attach({
        logging: {
          enabled: false
        },
        pageTracking: {
          enabled: true,
          // Optional. By default it gets the title from payload.instruction.config.title.
          getTitle: (payload) => {
            // For example, if you want to retrieve the tile from the document instead override with the following.
            return 'apply flyer';
          },
          // Optional. By default it gets the URL fragment from payload.instruction.fragment.
          getUrl: (payload) => {
            // For example, if you want to get full URL each time override with the following.
            return window.location.href;
          }
        },
        clickTracking: {
          // Set to `false` to disable in non-production environments.
          enabled: true,
          // Optional. By default it tracks clicks on anchors and buttons.
          filter: (element) => {
            // For example, if you want to also track clicks on span elements override with the following.
            return element instanceof HTMLElement &&
              (element.nodeName.toLowerCase() === 'a' ||
                element.nodeName.toLowerCase() === 'button' ||
                element.nodeName.toLowerCase() === 'span');
          }
        },
        exceptionTracking: {
          // Set to `false` to disable in non-production environments.
          enabled: true
        }
      });
    });
  }

  aurelia.use
    .standardConfiguration()
    .feature(PLATFORM.moduleName('resources/index'));

  // Uncomment the line below to enable animation.
  // aurelia.use.plugin(PLATFORM.moduleName('aurelia-animator-css'));
  // if the css animator is enabled, add swap-order="after" to all router-view elements

  // Anyone wanting to use HTMLImports to load views, will need to install the following plugin.
  // aurelia.use.plugin(PLATFORM.moduleName('aurelia-html-import-template-loader'));

  if (environment.debug) {
    aurelia.use.developmentLogging();
  }

  if (environment.testing) {
    aurelia.use.plugin(PLATFORM.moduleName('aurelia-testing'));
  }

  return aurelia.start().then(() => aurelia.setRoot(PLATFORM.moduleName('app')));
}
