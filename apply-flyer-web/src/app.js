import { inject, PLATFORM } from 'aurelia-framework';
import { FoundationService } from './services/foundation-service';

@inject(FoundationService)
export class App {
  constructor(foundationService) {
    this.foundationService = foundationService;
  }

  configureRouter(config, router) {
    config.title = 'Apply';
    this.router = router;
    config.options.pushState = true

    config.mapUnknownRoutes((i) => {
      return { route: '', moduleId: 'index' };
    });

    config.map([
      { route: ':id', moduleId: PLATFORM.moduleName('index'), name: 'foundations'},
    ]);

  }

}
